const getData = require('./wordnik');

const url1 = 'https://api.wordnik.com/v4/word.json/';
const define = '/definitions?limit=200&includeRelated=false&useCanonical=false&includeTags=false&api_key=';
const apikey = '08f4619e61fd50897e38c0780560e761770e8ca4949922fba';
const synonym = '/relatedWords?useCanonical=false&relationshipTypes=synonym&api_key=';
const antonym = '/relatedWords?useCanonical=false&relationshipTypes=antonym&api_key=';
const example = '/examples?includeDuplicates=false&useCanonical=false&api_key=';

let url;
let DefineData = [];
// let newExampleData;

module.exports = {
  getDefinition(word) {
    return new Promise((resolve, reject) => {
      url = url1 + word + define + apikey;
      getData(url).then((newDefineData) => {
        DefineData = [];
        for (let i = 0; i < newDefineData.length; i += 1) {
          DefineData.push(newDefineData[i].text);
        }
        // console.log(DefineData);
        resolve(DefineData);
      });
    });
  },

  getSynAnt(word, input) {
    if (input === 2) {
      url = url1 + word + synonym + apikey;
    } else {
      url = url1 + word + antonym + apikey;
    }
    return new Promise((resolve, reject) => {
      getData(url)
        .then((synonymData) => {
          synonymData = synonymData || [];
          if (synonymData.length > 0) {
            synonymData = synonymData[0].words;
          }
          resolve(synonymData);
        });
    });
  },

  getExample(word) {
    const newExampleData = [];
    url = url1 + word + example + apikey;
    return new Promise((resolve, reject) => {
      getData(url)
        .then((exampleData) => {
          for (let i = 0; i < exampleData.examples.length; i += 1) {
            newExampleData.push(exampleData.examples[i].text); // +obj[i].text);
          }
          resolve(newExampleData);
        });
    });
  },

  getRandomWord() {
    return new Promise((resolve, reject) => {
      url = `https://api.wordnik.com/v4/words.json/randomWord?hasDictionaryDef=true&maxCorpusCount=-1&minDictionaryCount=100&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=${apikey}`;
      getData(url).then((newInput) => {
        resolve(newInput.word);
      });
    });
  },
};
