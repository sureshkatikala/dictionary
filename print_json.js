module.exports = function printJSONWithIndex(JSON) {
  return new Promise((resolve, reject) => {
    if (JSON.length !== 0) {
      JSON.forEach((val, index) => {
        console.log(index+1 +". "+val);
      });
    } else {
      console.log('No Data Available');
    }
    resolve();
  });
};
