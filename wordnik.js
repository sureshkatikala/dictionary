const request = require('request');


module.exports = function getData(url) {
  const head = {
    url,
    json: true,
  };
  return new Promise((resolve, reject) => {
    request(head, (error, response, body) => {
      if (response.statusCode === 200 && !error) {
        resolve(body);
      } else {
        reject();
      }
    });
  });
};
