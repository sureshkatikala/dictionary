const readline = require('readline-sync');

const dictionary = require('./dictionary.js');

const app = require('./app.js');

const DEFINITION = 1;
const SYNONYM = 2;
const ANTONYM = 3;


let rand;

function game(score) {
  wordData = new Object();
  wordData.score = score;
  dictionary.getRandomWord().then((word) => {
    // console.log(wordData.word);
    wordData.word = word;
    getWordData(wordData).then(({ wordData, dataIndexs }) => {
      displayHint(wordData, dataIndexs).then((dataIndexs) => {
        console.log(`Your Score is : ${wordData.score}`);
        startGame(wordData, dataIndexs);
      });
    });
  });
}

function getWordData(wordData) {
  return new Promise((resolve) => {
    dataIndexs = {
      randomNumber: [],
      synonymIndex: 0,
      antonymIndex: 0,
      definitionIndex: 0,
    };
    Promise.all([dictionary.getDefinition(wordData.word), dictionary.getSynAnt(wordData.word, 2), dictionary.getSynAnt(wordData.word, 3)])
      .then((values) => {
        dataIndexs.randomNumber.push(DEFINITION);
        [wordData.DefineData, wordData.synonymData, wordData.antonymData] = values;
        if (wordData.synonymData.length > 0) {
          dataIndexs.randomNumber.push(SYNONYM);
        }
        if (wordData.antonymData.length > 0) {
          dataIndexs.randomNumber.push(ANTONYM);
        }
        resolve({ wordData, dataIndexs });
      });
  });
}

function displayHint(wordData, dataIndexs) {
  return new Promise((resolve) => {
    if (dataIndexs.randomNumber.length !== 0) {
      const rand = dataIndexs.randomNumber[Math.floor(Math.random() * dataIndexs.randomNumber.length)];
      let location;

      if (rand === 1 && dataIndexs.definitionIndex < wordData.DefineData.length) {
        console.log(`Definition : ${wordData.DefineData[dataIndexs.definitionIndex++]}`);
        if (dataIndexs.definitionIndex === wordData.DefineData.length) {
          // if there are no more defintions to show
          location = dataIndexs.randomNumber.indexOf(DEFINITION);
          if (location > -1) {
            // remove DEFINITION in array
            dataIndexs.randomNumber.splice(location, 1);
          }
        }
      } else if (rand === 2 && dataIndexs.synonymIndex < wordData.synonymData.length) {
        console.log(`Synonym : ${wordData.synonymData[dataIndexs.synonymIndex++]}`);
        if (dataIndexs.synonymIndex === wordData.synonymData.length) {
          // if there are no more synonyms to show
          location = dataIndexs.randomNumber.indexOf(SYNONYM);
          if (location > -1) {
            // remove SYNONYM in array
            dataIndexs.randomNumber.splice(location, 1);
          }
        }
      } else if (rand === 3 && dataIndexs.antonymIndex < wordData.antonymData.length) {
        console.log(`Antonym : ${wordData.antonymData[dataIndexs.antonymIndex++]}`);
        if (dataIndexs.antonymIndex === wordData.antonymData.length) {
          // if there are no more antonyms to show
          location = dataIndexs.randomNumber.indexOf(ANTONYM);
          if (location > -1) {
            // remove value ANTONYM in array
            dataIndexs.randomNumber.splice(location, 1);
          }
        }
      }
    } else { // shuffling the word
      const word = wordData.word;
      shuffledword = word.split('');
      for (let i = shuffledword.length - 1; i > 0; i -= 1) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffledword[i], shuffledword[j]] = [shuffledword[j], shuffledword[i]];
      }
      shuffledword = shuffledword.join('');
      console.log(shuffledword);
    }
    resolve(dataIndexs);
  });
}

function isCorrect(guessWord, wordData, dataIndexs) {
  return (guessWord === wordData.word || wordData.synonymData.indexOf(guessWord) > dataIndexs.synonymIndex - 1);
}

function startGame(wordData, dataIndexs) {
  const guessWord = (readline.question('Guess the word : ')).toLowerCase();
  console.log('--------------------------');
  if (isCorrect(guessWord, wordData, dataIndexs)) {
    wordData.score += 10;
    console.log(`Congratulations You have entered correct answer!!! \nYour Score is : ${wordData.score}`);
    console.log(`word is : ${wordData.word}`);
    app.buildURL(wordData.word, 5).then(() => {
      game(wordData.score);
    });
  } else {
    wordData.score -= 2;
    console.log(`oh no..!! Your Score is : ${wordData.score}`);
    return askUserForOptions({ wordData, dataIndexs });
  }
}

function askUserForOptions({ wordData, dataIndexs }) {
  const userinput = Number(readline.question('1.Try Again\t2.Hint\t3.Skip\t4.Quit\n'));
  console.log('--------------------------');
  if (userinput === 1) {
    startGame(wordData, dataIndexs);
  } if (userinput === 2) {
    wordData.score -= 3;
    console.log(`Your Score is : ${wordData.score}`);
    displayHint(wordData, dataIndexs).then(() => startGame(wordData, dataIndexs));
  } else if (userinput === 3) {
    wordData.score -= 4;
    console.log(`Your Score is : ${wordData.score}`);
    console.log(`word is : ${wordData.word}`);
    console.log(wordData.word);
    app.buildURL(wordData.word, 5)
      .then(() => {
        game(wordData.score);
      });
  } else if (userinput === 4) {
    console.log(`Your Total Score is : ${wordData.score}`);
  }
}

module.exports.game = game;

/* wordData ={
    DefineData: {},
    synonymData: {},
    antonymData: {},
    word: '',
  };
  */
/*
module.exports = function game(word, score) {
  const apikey = '08f4619e61fd50897e38c0780560e761770e8ca4949922fba';
  let synonymIndex = 0,
    antonymIndex = 0,
    definitionIndex = 0;
  const randomNumber = [DEFINITION];
  // random number array keeps track if availability of defintions synonyms and antonyms of a word
  // DEFINITION is put in array as all words have definitions
  let rand;
  let DefineData,
    synonymData,
    antonymData;
  const url = `https://api.wordnik.com/v4/words.json/randomWord?hasDictionaryDef=true&maxCorpusCount=-1&minDictionaryCount=100&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=${apikey}`;
  Promise.all([dictionary.getDefinition(word), dictionary.getSynAnt(word, 2), dictionary.getSynAnt(word, 3)])
    .then((values) => {
      [DefineData, synonymData, antonymData] = values;
      if (synonymData.length > 0) {
        randomNumber.push(SYNONYM);
      }
      if (antonymData.length > 0) {
        randomNumber.push(ANTONYM);
      }

      rand = randomNumber[Math.floor(Math.random() * randomNumber.length)];
      let location;
      console.log(`Your Score is : ${score}`);
      if (rand === 1 && definitionIndex < DefineData.length) {
        console.log(`Definition : ${DefineData[definitionIndex++]}`);
        if (definitionIndex === DefineData.length) {
          // if there are no more defintions to show
          location = randomNumber.indexOf(DEFINITION);
          if (location > -1) {
            // remove DEFINITION in array
            randomNumber.splice(location, 1);
          }
        }
      } else if (rand === 2 && synonymIndex < synonymData.length) {
        console.log(`Synonym : ${synonymData[synonymIndex++]}`);
        if (synonymIndex === synonymData.length) {
          // if there are no more synonyms to show
          location = randomNumber.indexOf(SYNONYM);
          if (location > -1) {
            // remove SYNONYM in array
            randomNumber.splice(location, 1);
          }
        }
      } else if (rand === 3 && antonymIndex < antonymData.length) {
        console.log(`Antonym : ${antonymData[antonymIndex++]}`);
        if (antonymIndex === antonymData.length) {
          // if there are no more antonyms to show
          location = randomNumber.indexOf(ANTONYM);
          if (location > -1) {
            // remove value ANTONYM in array
            randomNumber.splice(location, 1);
          }
        }
      }
      const fail = true;
      while (fail) {
        validate(fail, DefineData, synonymData, antonymData, definitionIndex, synonymIndex, antonymIndex, randomNumber, score, word)
          .then((fail, score)=> {
            this.fail = fail;
            this.score = score;
          });
      }
      getData(url).then((newInput) => {
        game(newInput.word, score);
      });
    });
};

function validate(fail, DefineData, synonymData, antonymData, definitionIndex, synonymIndex, antonymIndex, randomNumber, score, word) {
  // repeat guessing the word until user gives a correct answer or skips the word
  return new Promise((resolve, reject) => {
    let shuffledword;
    let location;
    const guessWord = (readline.question('Guess the word : ')).toLowerCase();
    console.log('--------------------------');
    if (guessWord === word || synonymData.indexOf(guessWord) > synonymIndex - 1) {
    // enters if user enters the correct word or its synonym
      score += 10;
      console.log(`Congratulations You have entered correct answer!!! \nYour Score is : ${score}`);
      fail = false;
      console.log(`word is : ${word}`);
      app.buildURL(word, 5).then(() => {
        resolve(fail, score);
      }); // to print dictionary data
    } else {
      let userinput;
      score -= 2;
      console.log(`oh no..!! Your Score is : ${score}`);
      let wrongInput = true;
      // wrongInput is used to check if user selected correct option
      while (wrongInput) { // loop through untill user enters correct option
        userinput = Number(readline.question('1.Try Again\t2.Hint\t3.Skip\n'));
        console.log('--------------------------');
        if (userinput === 1 || userinput === 2 || userinput === 3) {
          wrongInput = false;
        } else {
          console.log('Enter Correct Input : ');
        }
      }
      let index;
      if (userinput === 2) { // if user wants a hint
        score -= 3;
        console.log(`Your Score is : ${score}`);
        if (randomNumber.length > 0) {
          index = randomNumber[Math.floor(Math.random() * randomNumber.length)];
          if (index === 1 && definitionIndex < DefineData.length) {
            console.log(`Definition : ${DefineData[definitionIndex++]}`);
            if (definitionIndex === DefineData.length) {
            // if there are no defintions to show
              location = randomNumber.indexOf(DEFINITION);
              if (location > -1) {
              // if value 1 which corresponds to definition is present in array
                randomNumber.splice(location, 1);
              // remove the value from array
              }
            }
          } else if (index === 2 && synonymIndex < synonymData.length) {
            console.log(`Synonym : ${synonymData[synonymIndex++]}`);
            if (synonymIndex === synonymData.length) {
            //  if there are no synonyms to show
              location = randomNumber.indexOf(SYNONYM);
              if (location > -1) {
              // if value 2 which corresponds to synonyms is present in array
                randomNumber.splice(location, 1);
              // remove value from the array
              }
            }
          } else if (index === 3 && antonymIndex < antonymData.length) {
            console.log(`Antonym : ${antonymData[antonymIndex++]}`);
            if (antonymIndex === antonymData.length) {
            //  if there are no Antonyms to show
              location = randomNumber.indexOf(ANTONYM);
              if (location > -1) {
              // if value 3 which corresponds to Antonyms is present in array
                randomNumber.splice(location, 1);
              // remove value from the array
              }
            }
          }
        } else { // shuffling the word
          shuffledword = word.split('');
          for (let i = shuffledword.length - 1; i > 0; i -= 1) {
            const j = Math.floor(Math.random() * (i + 1));
            [shuffledword[i], shuffledword[j]] = [shuffledword[j], shuffledword[i]];
          }
          shuffledword = shuffledword.join('');
          console.log(shuffledword);
        }
      } else if (userinput === 3) {
        fail = false;
        score -= 4;
        console.log(`Skipping the word \nYour score is : ${score}`);
        console.log(`word is : ${word}`);
        app.buildURL(word, 5).then(() => {
          resolve(fail, score);
        }); // to print dictionary data
      }
    }
  });
}
*/
