const readline = require('readline-sync');

const dateFormat = require('dateformat');

const game = require('./game.js').game;

const getData = require('./wordnik.js');

const dictionary = require('./dictionary.js');

const printJSONWithIndex = require('./print_json.js');

let word;
const input = Number(readline.question('Choose from below options: \n1.Define\n2.Synonym\n3.Antonym\n4.Examples\n5.Word Full Dictionary\n6.Word of the day\n7.Word Game\n'));
if (input < 6) {
  word = (readline.question('Enter the word : ')).toLowerCase();
  console.log('--------------------------');
}
const DEFINITION = 1;
const SYNONYM = 2;
const ANTONYM = 3;
const EXAMPLE = 4;
const DICTIONARY = 5;
const WORDOFTHEDAY = 6;
const GAME = 7;


function buildURL(word, input) {
  const apikey = '08f4619e61fd50897e38c0780560e761770e8ca4949922fba';
  // return new Promise((resolve, reject) => {
  let url;
  if (input === DEFINITION) {
    dictionary.getDefinition(word)
      .then((DefineData) => {
        console.log('Defintions :');
        printJSONWithIndex(DefineData);
      })
      .catch(() => console.log('Please check your internet connection'));
  }
  if (input === SYNONYM) {
    dictionary.getSynAnt(word, SYNONYM)
      .then((synonymData) => {
        console.log('Synonyms :');
        printJSONWithIndex(synonymData);
      })
      .catch(() => console.log('Please check your internet connection'));
  }
  if (input === ANTONYM) {
    dictionary.getSynAnt(word, ANTONYM)
      .then((antonymData) => {
        console.log('Antonyms :');
        printJSONWithIndex(antonymData);
      })
      .catch(() => console.log('Please check your internet connection'));
  }
  if (input === EXAMPLE) {
    dictionary.getExample(word)
      .then((exampleData) => {
        console.log('Examples :');
        printJSONWithIndex(exampleData);
      })
      .catch(() => {
        console.log('Please check your internet connection');
      });
  }
  if (input === DICTIONARY) {
    return new Promise((resolve, reject) => {
      Promise.all([dictionary.getDefinition(word), dictionary.getSynAnt(word, 2), dictionary.getSynAnt(word, 3), dictionary.getExample(word)])
        .then((values) => {
          console.log('Defintions :');
          printJSONWithIndex(values[0]).then(() => {
            console.log('----------------------------------');
            console.log('Synonyms :');
            printJSONWithIndex(values[1]).then(() => {
              console.log('----------------------------------');
              console.log('Antonyms :');
              printJSONWithIndex(values[2]).then(() => {
                console.log('----------------------------------');
                console.log('Examples :');
                printJSONWithIndex(values[3]);
                console.log('----------------------------------');
              })
                .catch(() => {
                  console.log('No Antonyms');
                });
            })
              .catch(() => {
                console.log('No Synonyms');
              });
          })
            .catch(() => {
              console.log('No Definitions');
            });
        })
        .catch(() => {
          console.log('No Values returned from Promise.all');
        });
      resolve();
    });
  }
  if (input === WORDOFTHEDAY) { // input 6 implies Word of the day
    const day = dateFormat(new Date(), 'yyyy-mm-dd');
    url = `https://api.wordnik.com/v4/words.json/wordOfTheDay?date=${day}&api_key=${apikey}`;
    getData(url).then((json) => { // Network call to fetch data
      console.log(`${json.word}= ${json.definitions[0].text}`);
    })
      .catch(() => {
        console.log('Please check your internet connection');
      });
  }
  if (input === GAME) { // input 7 implies Game
    console.log('Now, we enter the Game !!!');
    console.log('--------------------------');
    game(0);
    // sending random word and Score to the game function
  }
}

buildURL(word, input);

module.exports.buildURL = buildURL;
